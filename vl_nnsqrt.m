function y = vl_nnsqrt(x, param, varargin)
% VL_NNSQRT signed square-root feature
%
% Author: Subhransu Maji, Tsung-Yu Lin

thresh = param(1);

backMode = numel(varargin) > 0 && ~isstr(varargin{1}) ;
if backMode
  dzdy = varargin{1} ;
end

if backMode
    y = 0.5./sqrt(abs(x)+thresh);
    y = y.*dzdy;
else
    y = sign(x).*sqrt(abs(x));
end
